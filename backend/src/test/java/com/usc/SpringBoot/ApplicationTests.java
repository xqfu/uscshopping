package com.usc.SpringBoot;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.usc.controller.OrderController;
import com.usc.controller.ProductController;
import com.usc.controller.UserController;

@SpringBootTest
class ApplicationTests {
	@Test
	void contextLoads() {
	}

}
