package com.usc.SpringBoot;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.usc.controller.ProductController;
import com.usc.dao.ProductDao;
import com.usc.beans.Product;

@RunWith(SpringRunner.class)
@WebMvcTest(value = (ProductController.class))
class ProductControllerTest {
	@Autowired
	private MockMvc mockMvc;
	
//	@MockBean
//	ProductDao productDao;
	
	@Autowired
	ProductController productController;
	
	/*
//Product(String sku, String name, String description, BigDecimal unitPrice, String imageUrl)	getproducts()	
	@Test
	void testGetAllProduct() throws Exception {
		List<Product> products = new ArrayList<>();
		BigDecimal aPrice = new BigDecimal(1.1, MathContext.DECIMAL64);
		products.add(new Product("000-005","Test5","Test5", aPrice, ""));
//	    entityManager.persist(products);
//	    entityManager.flush();
		Mockito.when(productController.getproducts()).thenReturn(products);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/products").accept(MediaType.APPLICATION_JSON);
		mockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(1)));
		
		//mockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
		//mockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(1)));
		//.andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(4))
	}
*/
}
