package com.usc.SpringBoot;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.Test;


import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.usc.beans.Product;
import com.usc.beans.User;
import com.usc.dao.UserDao;

import net.minidev.json.JSONObject;

@SpringBootTest
@AutoConfigureMockMvc
class TestingWebApplicationTest {  //https://spring.io/guides/gs/testing-web/
	@Autowired
	private MockMvc mvc;

	@Autowired
	UserDao userDao;
//
//
//	@Autowired
//	private Product product;
	
	@Test
	public void shouldReturnDefaultMessage() throws Exception {
		//System.out.println("String="+mockMvc.perform(get("/")).andDo(print()).toString());
		this.mvc.perform(get("/logout")).andDo(print()).andExpect(status().isOk())
		.andExpect(MockMvcResultMatchers.jsonPath("$.code").value(200))
		.andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Logout successfully"));
	}

	@Test
	public void getCheckLoginNoSucceed() throws Exception {
		//System.out.println("String="+mockMvc.perform(get("/")).andDo(print()).toString());
		this.mvc.perform(get("/checklogin")).andDo(print())
		.andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
		.andExpect(MockMvcResultMatchers.jsonPath("$.code").value(400))
		.andExpect(MockMvcResultMatchers.jsonPath("$.message").value("success"));
	}
	
//	Mockito.when(productController.getproducts()).thenReturn(products);
//	RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/products").accept(MediaType.APPLICATION_JSON);
//	mockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(1)));
	
	@Test
	public void getAllProductsAPI() throws Exception 
	{
	  mvc.perform(MockMvcRequestBuilders
	  			.get("/products"))
	      .andDo(print())
	      .andExpect(status().isOk())
	      .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(4))
	      .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1))
	      .andExpect(MockMvcResultMatchers.jsonPath("$[0].sku").value("000-0001"))
	      .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Test"))	
	      .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(2))
	      .andExpect(MockMvcResultMatchers.jsonPath("$[1].sku").value("000-0002"))
	      .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value("Pen"));   
	}
	
	@Test
	public void getProductByskuAPI() throws Exception 
	{
	  mvc.perform(MockMvcRequestBuilders
	  			.get("/productId").param("sku", "000-0001"))
	      .andDo(print())
	      .andExpect(status().isOk())
	      .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
	      .andExpect(MockMvcResultMatchers.jsonPath("$.sku").value("000-0001"))
	      .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Test"));   
	  //
      //.andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
	}
	
	@Test
	public void getProductByidAPI() throws Exception 
	{
	  mvc.perform(MockMvcRequestBuilders
	  			.get("/productdetail").param("id", "1"))
	      .andDo(print())
	      .andExpect(status().isOk())
	      .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
	      .andExpect(MockMvcResultMatchers.jsonPath("$.sku").value("000-0001"))
	      .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Test"));   
	  //
      //.andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
	}
	
	@Test
	public void getAllOrdersAPI() throws Exception 
	{
	  mvc.perform(MockMvcRequestBuilders
	  			.get("/orders"))
	      .andDo(print())
	      .andExpect(status().isOk())
	      .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(16))
	      .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1))
	      .andExpect(MockMvcResultMatchers.jsonPath("$[0].userId").value(52))
	      .andExpect(MockMvcResultMatchers.jsonPath("$[0].totalPrice").value(9.99));	
//	      .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(5))
//	      .andExpect(MockMvcResultMatchers.jsonPath("$[1].userId").value(2))
//	      .andExpect(MockMvcResultMatchers.jsonPath("$[1].totalPrice").value(9.99));	 getOrderDetailByOrderId
	}
	
	@Test
	public void getOrdersByusernameAPI() throws Exception 
	{  
	  mvc.perform(MockMvcRequestBuilders
	  			.get("/orderlist").param("username" , "ss20"))
	      .andDo(print())
	      .andExpect(status().isOk())
	      .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
	      .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1))
	      .andExpect(MockMvcResultMatchers.jsonPath("$[0].userId").value(52))
	      .andExpect(MockMvcResultMatchers.jsonPath("$[0].totalPrice").value(9.99));	
	}
	
	@Test
	public void getOrderDetailByorderIdAPI() throws Exception 
	{  
	  mvc.perform(MockMvcRequestBuilders
	  			.get("/orderdetails").param("orderId" , "1"))
	      .andDo(print())
	      .andExpect(status().isOk())
	      .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
	      .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1))
	      .andExpect(MockMvcResultMatchers.jsonPath("$[0].product.id").value(1))
	      .andExpect(MockMvcResultMatchers.jsonPath("$[0].amount").value(1));		
	}
	
	@Test
	public void getAllUsersAPI() throws Exception 
	{
	  mvc.perform(MockMvcRequestBuilders
	  			.get("/users"))
	      .andDo(print())
	      .andExpect(status().isOk())
	      .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(11))
	      .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(2))
	      .andExpect(MockMvcResultMatchers.jsonPath("$[0].username").value("xfu"))
	      .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(52))
	      .andExpect(MockMvcResultMatchers.jsonPath("$[1].username").value("ss20"));	 
	}
	
	@Test
	public void getUserIdbyusernameAPI() throws Exception 
	{
	  mvc.perform(MockMvcRequestBuilders
	  			.get("/userId").param("username" , "xfu"))
	      .andDo(print())
	      .andExpect(status().isOk())
	      .andExpect(MockMvcResultMatchers.jsonPath("$").value(2));	 
	}  //getUserNameById
	
	@Test
	public void getUserNameByIdbyuserIdAPI() throws Exception 
	{
	  mvc.perform(MockMvcRequestBuilders
	  			.get("/username").param("userId" , "2"))
	      .andDo(print())
	      .andExpect(status().isOk())
	      .andExpect(MockMvcResultMatchers.jsonPath("$").value("xfu"));	 
	}  // getUserRoleByName
	
	@Test
	public void getUserRolebyusernameAPI() throws Exception 
	{
	  mvc.perform(MockMvcRequestBuilders
	  			.get("/userRole").param("username" , "xfu"))
	      .andDo(print())
	      .andExpect(status().isOk())
	      .andExpect(MockMvcResultMatchers.jsonPath("$").value("ROLE_ADMIN"));	 
	}  //getUserNameById
	
	
	@Test
	public void deleteUserbyidAPI() throws Exception 
	{
//	  mvc.perform(MockMvcRequestBuilders
//	  			.get("/userRole").param("username" , "xfu"))
//	      .andDo(print())
//	      .andExpect(status().isOk())
//	      .andExpect(MockMvcResultMatchers.jsonPath("$").value("ROLE_ADMIN"));	
//	  
	  mvc.perform(MockMvcRequestBuilders.delete("/users/{id}", 552))
              .andExpect(status().isOk()); 
	} 
	
	@Test
	public void postRegisterUserAPI() throws Exception 
	{
	  User user = new User("newUser1","123456");
//	  JSONObject jsonObject = new JSONObject();
//      String strJson = jsonObject.toString(); MvcResult mvcResult = 
	      ObjectMapper objectMapper = new ObjectMapper();
	      String json = objectMapper.writeValueAsString(user);
	      mvc.perform(
	              MockMvcRequestBuilders.post("/users").contentType(MediaType.APPLICATION_JSON)
	              .content(json))
	      .andExpect(status().isOk())
	      .andReturn();
	}
	
	public static String asJsonString(final Object obj) {
	    try {
	        final ObjectMapper mapper = new ObjectMapper();
	        final String jsonContent = mapper.writeValueAsString(obj);
	        return jsonContent;
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}  
	
	@Test
	public void putUserAPI() throws Exception 
	{  //(String sku, String name, String description, BigDecimal unitPrice, String imageUrl)
//	  mvc.perform(MockMvcRequestBuilders
//	  			.get("/userRole").param("username" , "xfu"))
//	      .andDo(print())
//	      .andExpect(status().isOk())
//	      .andExpect(MockMvcResultMatchers.jsonPath("$").value("ROLE_ADMIN"));	
		//UserDao userDao;
		User user = new User();
		 user = userDao.findByUsername("bob");
		 user.setEmail("bob@gmail.com");
		 //System.out.println("user.getId="+user.getId()); 
         //.content(json))  	              .content(asJsonString(user)))
		 
//	      ObjectMapper objectMapper = new ObjectMapper();
//	      String json = objectMapper.writeValueAsString(user);
	      mvc.perform(
	              MockMvcRequestBuilders.put("/users").contentType(MediaType.APPLICATION_JSON)
	      .content(asJsonString(user)))
	      .andExpect(status().isOk())
	      .andReturn();
	      

	} 
	
//	@Test
//	public void shouldOrders() throws Exception {
//		//System.out.println("String="+mockMvc.perform(get("/")).andDo(print()).toString());
//		this.mockMvc.perform(get("/orders")).andDo(print()).andExpect(status().isOk())
//		.andExpect(content().string(containsString("userId")));
//	}
}
