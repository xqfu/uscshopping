package com.usc.SpringBoot;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.usc.controller.AuthController;
import com.usc.controller.OrderController;
import com.usc.controller.ProductController;
import com.usc.controller.UserController;

@SpringBootTest
class SmokeTest {
	@Autowired
	AuthController authController;
	
	
	@Autowired
	OrderController orderController;
	
	@Autowired
	ProductController productController;
	
	@Autowired
	UserController userController;
	
	@Test
	void contextLoads() {
		assertThat(authController).isNotNull();
		assertThat(orderController).isNotNull();
		assertThat(productController).isNotNull();
		assertThat(userController).isNotNull();
	}
}
