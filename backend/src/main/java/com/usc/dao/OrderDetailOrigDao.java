package com.usc.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.usc.beans.OrderDetailOrig;

public interface OrderDetailOrigDao extends JpaRepository<OrderDetailOrig, Integer>{
	List<OrderDetailOrig> findByOrderId(int orderId);

}
