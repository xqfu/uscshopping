package com.usc.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.usc.beans.Order;

public interface OrderDao extends JpaRepository<Order, Integer>{
	List<Order> findByUserId(int userId);

//	@Query(value = "SELECT max(id) as maxId FROM order")
//	int maxId();
}