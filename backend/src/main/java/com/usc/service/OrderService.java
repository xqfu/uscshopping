package com.usc.service;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usc.dao.OrderDao;
import com.usc.beans.Order;
import com.usc.http.Response;

@Service
@Transactional
public class OrderService {
	@Autowired
	OrderDao orderDao;
	
	public Response addOrder(Order order) {
		order.setShipped(false);
		order.setDateCreated(new Date());
		order.setTrackingNumber("");
		orderDao.save(order);
		int currentOrderId = order.getId();
		return new Response(true, ""+currentOrderId);
	}	
	
	public Response deleteOrder(int id) {
		if (orderDao.findById(id) != null) {
			orderDao.deleteById(id);
			return new Response(true);
			
		} else {
			return new Response(false, "Order is not found!");			
		}
	}

}
