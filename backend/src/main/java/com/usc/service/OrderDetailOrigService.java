package com.usc.service;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usc.beans.OrderDetailOrig;
import com.usc.dao.OrderDetailOrigDao;
import com.usc.http.Response;

@Service
@Transactional
public class OrderDetailOrigService {
	@Autowired
	OrderDetailOrigDao orderDetailOrigDao;
	
	public Response addOrderDetailOrig(OrderDetailOrig orderDetailOrig) {
		orderDetailOrigDao.save(orderDetailOrig);
		//int currentOrderDetailOrigId = orderDetailOrig.getId();
		return new Response(true);
	}	
}
