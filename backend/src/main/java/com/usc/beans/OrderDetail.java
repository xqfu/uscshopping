package com.usc.beans;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.springframework.data.jpa.repository.Query;


import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "order_detail")  //Hibernate Entity - match the java class to the db column name
//@Query("SELECT a.id, a.order_id, a.product_id, b.name as product_name, a.amount From order_detail a, product b where a.product_id = b.id")
public class OrderDetail {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO, generator="identity")
	@GenericGenerator(name = "identity", strategy = "native")
	private int id;
	
//order_id           int not null,
//product_id int null,
//amount             int null
    @Column(name = "order_id", nullable = false)
    private int orderId;

//    @ManyToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name="order_id", referencedColumnName="id") //
//    private Order order;
    
//    @Column(name = "product_id")
//    private int productId;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    private Product product;
    
    @Column(name = "amount")
    private int amount;
    
//    private float itemPrice;

//    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    @JoinColumn(name = "order_id", referencedColumnName = "id")
//    @JsonIgnoreProperties("oderDetailList")
//    private Order order;
	


	public OrderDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "OrderDetail [id=" + id + ", orderId=" + orderId + ", product=" + product + ", amount=" + amount + "]";
	}

	public OrderDetail(int id, int orderId, Product product, int amount) {
	super();
	this.id = id;
	this.orderId = orderId;
	this.product = product;
	this.amount = amount;
}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
    
//	public float getItemPrice() {
//		this.itemPrice =   
//		return itemPrice;
//	}
}
