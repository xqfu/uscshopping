package com.usc.beans;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "order_detail")  //Hibernate Entity - match the java class to the db column name
public class OrderDetail1N {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO, generator="identity")
	@GenericGenerator(name = "identity", strategy = "native")
	private int id;
	
  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name="order_id", referencedColumnName="id") //
  private Order order;
  
  @Column(name = "product_id")
  private int productId;
  
  @Column(name = "amount")
  private int amount;

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Order getOrder() {
		return order;
	}
	
	public void setOrder(Order order) {
		this.order = order;
	}
	
	public int getProductId() {
		return productId;
	}
	
	public void setProductId(int productId) {
		this.productId = productId;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	@Override
	public String toString() {
		return "OrderDetail1N [id=" + id + ", order=" + order + ", productId=" + productId + ", amount=" + amount + "]";
	}

	public OrderDetail1N(int id, Order order, int productId, int amount) {
		super();
		this.id = id;
		this.order = order;
		this.productId = productId;
		this.amount = amount;
	}

	public OrderDetail1N() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
  
}
