package com.usc.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "order_detail")  //Hibernate Entity - match the java class to the db column name
public class OrderDetailOrig {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO, generator="identity")
	@GenericGenerator(name = "identity", strategy = "native")
	private int id;
	
    @Column(name = "order_id", nullable = false)
    private int orderId;
    
    @Column(name = "product_id")
    private int productId;
    
    @Column(name = "amount")
    private int amount;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "OrderDetailOrig [id=" + id + ", orderId=" + orderId + ", productId=" + productId + ", amount=" + amount
				+ "]";
	}

	public OrderDetailOrig(int id, int orderId, int productId, int amount) {
		super();
		this.id = id;
		this.orderId = orderId;
		this.productId = productId;
		this.amount = amount;
	}

	public OrderDetailOrig() {
		super();
		// TODO Auto-generated constructor stub
	}
    
   
}
