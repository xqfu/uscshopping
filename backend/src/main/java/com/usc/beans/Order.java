package com.usc.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "orders")  //Hibernate Entity - match the java class to the db column name
//@Immutable
//@Subselect("SELECT a.id, a.user_id, b.username, a.total_price, a.date_created FROM orders a, usc_user b WHERE a.user_id = b.id")
public class Order implements Serializable {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO, generator="identity")
	@GenericGenerator(name = "identity", strategy = "native")
	private int id;
	
//    user_id           int                                  not null,
//    total_price       float     default 0                  not null,
//    date              timestamp  default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
//    shipped           tinyint(1) default 0                 null,
//    tracking_number   varchar(80)     
	
    @Column(name = "user_id", nullable = false)
    private int userId;

   @Column(name = "orderusername")
    private String orderusername;

    @Column(name = "total_price", nullable = false)
    private BigDecimal totalPrice;

    @Column(name = "date_created")
    private Date dateCreated;

    @Column(name = "shipped")
    private boolean shipped;
    
    @Column(name = "tracking_number")
    private String trackingNumber;

//	@ManyToMany(fetch = FetchType.EAGER)
//	@JoinTable(name = "usc_user", joinColumns = {
//			@JoinColumn(name = "id", referencedColumnName = "user_id") }, inverseJoinColumns = {
//					@JoinColumn(name = "user_id", referencedColumnName = "id") })
//	private List<User> userList = new ArrayList<User>();	
	
//    @OneToMany(fetch = FetchType.EAGER, mappedBy = "order", cascade = CascadeType.ALL)
//    @JsonIgnoreProperties("order")
//    private List<OrderDetail> orderDetailList = new ArrayList<>();
	
    @OneToMany(mappedBy="order", cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private List<OrderDetail1N> orderDetail1NList;
    
	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}



	public String getOrderusername() {
		return orderusername;
	}



	public void setOrderusername(String orderusername) {
		this.orderusername = orderusername;
	}



//	public List<OrderDetail1N> getOrderDetail1NList() {
//		return orderDetail1NList;
//	}
//
//
//
//	public void setOrderDetail1NList(List<OrderDetail1N> orderDetail1NList) {
//		this.orderDetail1NList = orderDetail1NList;
//	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
//	public int getUsername() {
//		return username;
//	}
//
//	public void setUsername(int username) {
//		this.username = username;
//	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Date getDateCreated() {
		//DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public boolean isShipped() {
		return shipped;
	}

	public void setShipped(boolean shipped) {
		this.shipped = shipped;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", userId=" + userId + ", orderusername=" + orderusername + ", totalPrice="
				+ totalPrice + ", dateCreated=" + dateCreated + ", shipped=" + shipped + ", trackingNumber="
				+ trackingNumber + ", orderDetail1NList=" + orderDetail1NList + "]";
	}

	public Order(int id, int userId, int username, BigDecimal totalPrice, Date dateCreated, boolean shipped,
			String trackingNumber) {
		super();
		this.id = id;
		this.userId = userId;
		this.totalPrice = totalPrice;
		this.dateCreated = dateCreated;
		this.shipped = shipped;
		this.trackingNumber = trackingNumber;
	}

	public Order(int id, int userId, int username, BigDecimal totalPrice) {
		super();
		this.id = id;
		this.userId = userId;
		this.totalPrice = totalPrice;
		this.dateCreated = new Date();
		this.shipped = false;
		this.trackingNumber = "";
	}



	public Order(int id, int userId, String orderusername, BigDecimal totalPrice, Date dateCreated, boolean shipped,
			String trackingNumber, List<OrderDetail1N> orderDetail1NList) {
		super();
		this.id = id;
		this.userId = userId;
		this.orderusername = orderusername;
		this.totalPrice = totalPrice;
		this.dateCreated = dateCreated;
		this.shipped = shipped;
		this.trackingNumber = trackingNumber;
		this.orderDetail1NList = orderDetail1NList;
	}

}
