package com.usc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usc.beans.Product;
import com.usc.dao.ProductDao;
//import com.usc.service.ProductService;

@RestController() // recept API request
@RequestMapping("/productId")
public class ProductIdController {
	@Autowired
	ProductDao productDao;
	
	@GetMapping
	public Product getProductBySku(String sku) {
	Product product = productDao.findBySku(sku);
	return product;
}
	
//	public int getProductIdBySku(String sku) {
//		Product product = productDao.findBySku(sku);
//		return product.getId();
//	}
	

}
