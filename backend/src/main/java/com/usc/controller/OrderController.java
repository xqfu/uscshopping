package com.usc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usc.beans.Order;
//import com.usc.beans.User;
import com.usc.dao.OrderDao;
import com.usc.dao.OrderSearchDao;
//import com.usc.dao.UserDao;
import com.usc.http.Response;
import com.usc.service.OrderService;

@RestController() // recept API request
@RequestMapping("/orders")
public class OrderController {
	@Autowired
	OrderDao orderDao;

	@Autowired
	OrderSearchDao orderSearchDao;

//	@Autowired
//	OrderMaxDao orderMaxDao;
	
	@Autowired
	OrderService orderService;

//	@PreAuthorize("hasAuthority('ROLE_ADMIN', 'ROLE_USER')")
	@GetMapping
	public List<Order> getOrders() {
		return orderDao.findAll();
	}
	
	@GetMapping("/search")
	public List<Order> getordersByDates(String cri1, String cri2) {
		return orderSearchDao.fullTextSearch(cri1, cri2);
	}
	
//	@GetMapping("/maxId")
//	public int getOrdermaxId() {
//		return orderMaxDao.maxId();
//	}
	
	@PostMapping
	public Response addOrder(@RequestBody Order order) { // json
		return orderService.addOrder(order);
	}
	
	@DeleteMapping("/{id}")
	public Response deleteOrder(@PathVariable int id) { 
		return orderService.deleteOrder(id);
	}
}
