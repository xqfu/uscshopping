package com.usc.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usc.beans.User;
import com.usc.dao.UserDao;

@RestController() // recept API request
@RequestMapping("/username")
public class UsernameController {
	@Autowired
	UserDao userDao;
	
	@GetMapping
	public String getUserNameById(int userId) {
		Optional<User> user = userDao.findById(userId);
		return user.get().getUsername();
	}
}
