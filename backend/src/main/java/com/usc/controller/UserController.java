package com.usc.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.usc.beans.User;
import com.usc.dao.UserDao;
import com.usc.dao.UserSearchDao;
import com.usc.http.Response;
import com.usc.service.UserService;

@RestController() // recept API request
@RequestMapping("/users")
public class UserController {
	@Autowired
	UserDao userDao;
	
	@Autowired
	UserSearchDao userSearchDao;
	
	@Autowired
	UserService userService;

	@Autowired
	PasswordEncoder passwordEncoder;
	
//	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	@GetMapping
	public List<User> getusers() {
		return userDao.findAll();
	}
	
	@GetMapping("/username")
	public List<User> getuserByUsername(String username) {
		List<User> userList = new ArrayList<>();
		userList.add(userDao.findByUsername(username));
		return userList;
	}
	

	
	@GetMapping("/usernameOne")
	public User getOneUserByUsername(String username) {
		return userDao.findByUsername(username);
	}
	@GetMapping("/search")
	public List<User> getusersByName(String cri) {
		return userSearchDao.fullTextSearch(cri);
	}
	
	@PostMapping
	public Response addUser(@RequestBody User user) { // json
		return userService.register(user);
	}

	//@PreAuthorize("hasAuthority('ROLE_ADMIN', 'ROLE_USER')")
	@PutMapping
	public Response changeUser(@RequestBody User user) { 
		return userService.changeUser(user);
	//return userService.changePassword(user, authentication);
	}	
	
	@PutMapping("/changepwd")
	public Response changePassword(@RequestBody User user) { 
		return userService.changePassword(user);
	    //return userService.changePassword(user, authentication);
	}	
	
//	public Response changeUser(@RequestBody User user, Authentication authentication) { 
//		return userService.changeUserorPassword(user, authentication);
//		//return userService.changePassword(user, authentication);
//	}
	
	@DeleteMapping("/{id}")
	public Response deleteUser(@PathVariable int id) { 
		return userService.deleteUser(id);
	}
	

	
//	@RequestMapping(value = "/user", method = RequestMethod.GET)
//	public String userpage(Model model) {
//	    Authentication auth = SecurityContextHolder.getContext()
//	        .getAuthentication();
//	    String name = auth.getName();
//	    User user = userManager.findUserByEmail(name);
//	    model.addAttribute("name", name);
//	    model.addAttribute("userid", user.getId());
//	    return "user";
//	}
}
