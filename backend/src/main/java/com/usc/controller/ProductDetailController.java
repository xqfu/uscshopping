package com.usc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usc.beans.OrderDetail;
import com.usc.beans.Product;
import com.usc.dao.ProductDetailDao;

@RestController() // recept API request
@RequestMapping("/productdetail")
public class ProductDetailController {

	@Autowired
	ProductDetailDao productDetailDao;
	
	@GetMapping
	public Product getProductDetailById(int id) {
		Product productDetail = productDetailDao.findById(id);
		return productDetail;
	}
	
//	@GetMapping
//	public List<OrderDetail> getOrderDetailByOrderId(int orderId) {
//		return orderDetailDao.findByOrderId(orderId);
}
