package com.usc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usc.beans.OrderDetail;
import com.usc.dao.OrderDetailDao;

@RestController() // recept API request
@RequestMapping("/orderdetails")
public class OrderDetailController {
	@Autowired
	OrderDetailDao orderDetailDao;
	
	@GetMapping
	public List<OrderDetail> getOrderDetailByOrderId(int orderId) {
		return orderDetailDao.findByOrderId(orderId);
	}
}
