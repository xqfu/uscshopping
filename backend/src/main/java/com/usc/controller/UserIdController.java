package com.usc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usc.beans.User;
import com.usc.dao.UserDao;

@RestController() // recept API request
@RequestMapping("/userId")
public class UserIdController {
	@Autowired
	UserDao userDao;
	
	@GetMapping
	public int getUserIdByName(String username) {
		User user = userDao.findByUsername(username);
		return user.getId();
	}
}
