package com.usc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usc.beans.Order;
import com.usc.beans.User;
import com.usc.dao.OrderDao;
import com.usc.dao.UserDao;
import com.usc.service.UserService;

@RestController() // recept API request
@RequestMapping("/orderlist")
public class OrderListController {
	@Autowired
	OrderDao orderDao;

	@Autowired
	UserDao userDao;
	
	@Autowired
	UserService userService;
	
	@GetMapping
	public List<Order> getOrderListByName(String username) {
		System.out.println("username="+username);
		User user = userDao.findByUsername(username);
//		int userId = user.getId();
//		return orderDao.findByUserId(userId);
		System.out.println("isUserAdmin(user)="+userService.isUserAdmin(user));	
        if(userService.isUserAdmin(user)) {
        	System.out.println("isUserAdmin");
            return (List<Order>)orderDao.findAll();
        } else {
        	System.out.println("not isUserAdmin");
            return orderDao.findByUserId(userDao.findByUsername(username).getId());
        }
	}
}
