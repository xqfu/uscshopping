package com.usc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usc.beans.User;
import com.usc.dao.UserDao;
import com.usc.service.UserService;
import com.usc.http.Response; 

@RestController() // recept API request
@RequestMapping("/userRole")
public class UserRoleController {
	@Autowired
	UserDao userDao;
	
	@Autowired
	UserService userService;

	
	@GetMapping
	public String getUserRoleByName(String username) {
		User user = userDao.findByUsername(username);
		if (userService.isUserAdmin(user)) return "ROLE_ADMIN";
		return "ROLE_USER";
	}
//	@GetMapping
//	public boolean isAdminByName(String username) {
//		User user = userDao.findByUsername(username);
//		return userService.isUserAdmin(user);
//	}
//	@GetMapping
//	public Response isAdminByName(String username) {
//		User user = userDao.findByUsername(username);
//		return new Response(userService.isUserAdmin(user));
//	}
}
