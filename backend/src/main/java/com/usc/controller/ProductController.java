package com.usc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usc.beans.Product;
import com.usc.dao.ProductDao;
import com.usc.dao.ProductDetailDao;
import com.usc.dao.ProductSearchDao;
import com.usc.http.Response;
import com.usc.service.ProductService;

@RestController() // recept API request
@RequestMapping("/products")
public class ProductController {
	@Autowired
	ProductDao productDao;
	
	@Autowired
	ProductDetailDao productDetailDao;

	@Autowired
	ProductSearchDao productSearchDao;
	
	@Autowired
	ProductService productService;

//	@PreAuthorize("hasAuthority('ROLE_ADMIN', 'ROLE_USER')")
	@GetMapping
	public List<Product> getproducts() {
		return productDao.findAll();
	}
	
	@GetMapping("/detail")
	public Product getProductDetailById(int id) {
		Product productDetail = productDetailDao.findById(id);
		return productDetail;
	}
	

	@GetMapping("/search")
	public List<Product> getproductsByNameDesc(String cri) {
		return productSearchDao.fullTextSearch(cri);
	}
	
	
  	@PostMapping
	public Response addProduct(@RequestBody Product product) { // json
		return productService.addProduct(product);
	}
  	
	@PutMapping
	public Response changeProduct(@RequestBody Product product) { 
		return productService.changeProduct(product);
	}
	
	@DeleteMapping("/{id}")
	public Response deleteProduct(@PathVariable int id) { 
		return productService.deleteProduct(id);
	}
}
