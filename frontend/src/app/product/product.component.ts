//@ts-nocheck
import { Component, VERSION, OnInit, Input, Output, EventEmitter, OnChanges, forwardRef  } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { AuthService } from '../services/auth.service';
import { CartService } from 'src/app/services/cart.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';
//  https://www.delftstack.com/zh/howto/angular/pagination-in-angular/
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})

export class ProductComponent implements OnInit, OnChanges {
  response;

  constructor(public cartService: CartService, public authService: AuthService,
    private router: Router) { }

  pages: number = 1;
  dataset: any[] = ['Product 1 Description ','Product 2 Description ','Product 3 Description ','Product 4 Description ','Product 5 Description ','Product 6 Description ','Product 7 Description ','Product 8 Description ','Product 9 Description ','Product 10 Description ','Product 11 Description ','Product 12 Description ','Product 13 Description ','Product 14 Description ','Product 15 Description ','Product 16 Description ','Product 17 Description ','Product 18 Description ','Product 19 Description ','Product 20 Description '];

  
  productList = [
    {name: 'Z900', price: 8799},
    {name: 'shubert helmet', price: 999},
    {name: 'sport gloves', price: 99}
   ];
  cartProducts = [];

  addToCart(product) {
    // console.log(this.product);
    this.cartService.addToCart(product);
  }

  toCart() {
    this.response = res;
    this.router.navigate(['/product']);
  }
}
