//@ts-nocheck
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig } from './app.config';
import { BehaviorSubject, Observable, pipe, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private API_URL = AppConfig.API_URL;
  constructor(private httpClient: HttpClient) { }

  private getProducts(searchUrl: string): Observable<Product[]> {
    return this.httpClient.get<GetResponseProducts>(searchUrl).pipe(
      map(response => response._embedded.products)
    );
  }

  //returns an observable of product array
  //maps json data from spring rest to product array
  getProductList(theCategoryId: number): Observable<Product[]> {

    //build URL based on category id
    const searchUrl = `${this.baseUrl}/search/findByCategoryId?id=${theCategoryId}`;

  return this.getProducts(searchUrl);
  }

  //add support for pagination
  getProductListPaginate(thePage: number, thePageSize: number,
                            theCategoryId: number): Observable<GetResponseProducts> {

    //build URL based on category id for pagination
    //Spring REST API supports pagination out the box, so just pass page # and size
    const searchUrl = `${this.baseUrl}/search/findByCategoryId?id=${theCategoryId}`
                    + `&page=${thePage}&size=${thePageSize}`;
  return this.httpClient.get<GetResponseProducts>(searchUrl);
  }


  getProductCategories(): Observable<ProductCategory[]> {
    return this.httpClient.get<GetResponseProductCategory>(this.categoryUrl).pipe(
      map(response => response._embedded.productCategory)
      );
  }

  searchProducts(theKeyword: string): Observable<Product[]> {
    const searchUrl = `${this.baseUrl}/search/findByNameContaining?name=${theKeyword}`;
    return this.getProducts(searchUrl);
  }

  searchProductsPaginate(thePage: number, 
                        thePageSize: number,
                        theKeyword: string): Observable<GetResponseProducts> {

    //build URL based on keyword
    //Spring REST API supports pagination out the box, so just pass page # and size
    const searchUrl = `${this.baseUrl}/search/findByNameContaining?name=${theKeyword}`
                      + `&page=${thePage}&size=${thePageSize}`;
    return this.httpClient.get<GetResponseProducts>(searchUrl);
    }

  getProduct(theProductId: number): Observable<Product> {
    //build URL based on product id
    const productUrl = `${this.baseUrl}/${theProductId}`;
    return this.httpClient.get<Product>(productUrl);
  }


}

//helps unwrap JSON data
interface GetResponseProducts {
  _embedded: {
    products: Product[];
  },
  page: { //add pagination
    size: number,
    totalElements: number,
    totalPages: number,
    number: number

  }
}

