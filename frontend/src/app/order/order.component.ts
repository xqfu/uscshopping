//@ts-nocheck
import { Component, VERSION, OnInit, Input, Output, EventEmitter, OnChanges, forwardRef} from '@angular/core';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  pages: number = 1;
  dataset: any[] = ['Oder 1 Description ','Oder 2 Description ','Oder 3 Description ','Oder 4 Description ','Oder 5 Description ','Oder 6 Description ','Oder 7 Description ','Oder 8 Description ','Oder 9 Description ','Oder 10 Description ','Oder 11 Description ','Oder 12 Description ','Oder 13 Description ','Oder 14 Description ','Oder 15 Description ','Oder 16 Description ','Oder 17 Description ','Oder 18 Description ','Oder 19 Description ','Oder 20 Description '];
  
  constructor() { }

  ngOnInit(): void {
  }

}
